<?php

Route::namespace('Auth')->group(function(){
    Route::post('register', 'RegisterController');
    Route::post('login', 'LoginController');
    Route::post('logout', 'LogoutController');
});

Route::get('user', 'UserController');

Route::namespace('Perpus')->middleware('auth:api')->group(function(){
    Route::post('create-buku', 'BukuController@store');
    Route::patch('update-buku/{buku}', 'BukuController@update');
    Route::delete('delete-buku/{buku}', 'BukuController@destroy');

    Route::post('create-mahasiswa', 'MahasiswaController@store');
    Route::patch('update-mahasiswa/{mahasiswa}', 'MahasiswaController@update');
    Route::delete('delete-mahasiswa/{mahasiswa}', 'MahasiswaController@destroy');

    Route::post('create-pinjaman', 'PinjamanController@store');
    Route::patch('update-pinjaman/{pinjaman}', 'PinjamanController@update');
    Route::patch('pengembalian-pinjaman/{pinjaman}', 'PinjamanController@pengembalian')->middleware('login');
    Route::delete('delete-pinjaman/{pinjaman}', 'PinjamanController@destroy');
});

Route::get('bukus/{buku}', 'Perpus\BukuController@show');
Route::get('bukus', 'Perpus\BukuController@index');

Route::get('mahasiswas/{mahasiswa}', 'Perpus\MahasiswaController@show');
Route::get('mahasiswas', 'Perpus\MahasiswaController@index');

Route::get('pinjaman_bukus/{pinjaman}', 'Perpus\PinjamanController@show');
Route::get('pinjaman_bukus', 'Perpus\PinjamanController@index');