<?php

namespace App\Http\Controllers\Perpus;

use App\Http\Controllers\Controller;
use App\Http\Requests\BukuRequest;
use App\Http\Resources\BukuResources;
use App\Models\Perpus\Buku;
use Illuminate\Http\Request;

class BukuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bukus = Buku::get();
        return BukuResources::collection($bukus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BukuRequest $request)
    {
        $books = auth()->user()->bukus()->create($this->bukuStore());

        return $books;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Buku $buku)
    {
        return new BukuResources($buku);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Buku $buku)
    {
        $request->validate([
            'judul' => ['required'],
            'pengarang' => ['required'],
            'tahun_terbit' => ['required']
        ]);
        
        $buku->update($this->bukuStore());

        return new BukuResources($buku);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Buku $buku)
    {
        $buku->delete();
        return response()->json('Book Was Delete', 200);
    }

    public function bukuStore(){
        return [
            'kode_buku' => request('kode_buku'),
            'judul' => request('judul'),
            'pengarang' => request('pengarang'),
            'tahun_terbit' => request('tahun_terbit'),
        ];
    }
}
