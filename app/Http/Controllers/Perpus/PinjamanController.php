<?php

namespace App\Http\Controllers\Perpus;

use App\Http\Controllers\Controller;
use App\Http\Requests\PinjamanRequest;
use App\Http\Resources\PinjamanResources;
use App\Models\Perpus\PinjamanBuku;
use Illuminate\Http\Request;

class PinjamanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pinjamans = PinjamanBuku::get();
        return PinjamanResources::collection($pinjamans);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PinjamanRequest $request)
    {
        $pinjamans = auth()->user()->pinjamanBukus()->create($this->pinjamanStore());

        return $pinjamans;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PinjamanBuku $pinjaman)
    {
        return new PinjamanResources($pinjaman);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PinjamanBuku $pinjaman)
    {
        $request->validate([
            'buku_id' => ['required'],
            'mahasiswa_id' => ['required'],
            'tanggal_pinjaman' => ['date','required'],
            'tanggal_akhir_pinjaman' => ['date','required'],
        ]);

        $pinjaman->update($this->pinjamanStore());

        return new PinjamanResources($pinjaman);
        
    }

    public function pengembalian(Request $request, PinjamanBuku $pinjaman)
    {
        $request->validate([
            'buku_id' => ['required'],
            'mahasiswa_id' => ['required'],
            'tanggal_pengembalian' => ['date','required'],
            'status_ontime' => ['required'],
        ]);

        $pinjaman->update($this->pengembalianStore());

        return new PinjamanResources($pinjaman);
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PinjamanBuku $pinjaman)
    {
        $pinjaman->delete();
        return response()->json('Pinjaman Sudah Selesai', 200);
    }

    public function pinjamanStore(){
        return [
            'buku_id' => request('buku_id'),
            'mahasiswa_id' => request('mahasiswa_id'),
            'tanggal_pinjaman' => request('tanggal_pinjaman'),
            'tanggal_akhir_pinjaman' => request('tanggal_akhir_pinjaman'),
        ];
    }

    public function pengembalianStore(){
        return [
            'buku_id' => request('buku_id'),
            'mahasiswa_id' => request('mahasiswa_id'),
            'tanggal_pengembalian' => request('tanggal_pengembalian'),
            'status_ontime' => request('status_ontime'),
        ];
    }
}