<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BukuResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'kode_buku' => $this->kode_buku,
            'judul' => $this->judul,
            'pengarang' => $this->pengarang,
            'tahun_terbit' => $this->tahun_terbit,
            'user' => $this->user->name,
        ];
    }

    public function with($request)
    {
        return ['status' => 'Succes'];
    }
}
