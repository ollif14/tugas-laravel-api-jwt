<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PinjamanResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'buku_id' => $this->buku_id,
            'mahasiswa' => $this->mahasiswa_id,
            'tanggal_pinjaman' => $this->tanggal_pinjaman,
            'tanggal_akhir_pinjaman' => $this->tanggal_akhir_pinjaman,
            'tanggal_pengembalian' => $this->tanggal_pengembalian,
            'status_ontime' => $this->status_ontime,
            'user' => $this->user->name,
            'buku' => $this->buku->judul,
            'mahasiswa_peminjam' => $this->mahasiswa->nama,
        ];
    }

    public function with($request)
    {
        return ['status' => 'Succes'];
    }
}
