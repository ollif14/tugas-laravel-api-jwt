<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MahasiswaResources extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'nama' => $this->nama,
            'nim' => $this->nim,
            'fakultas' => $this->fakultas,
            'jurusan' => $this->jurusan,
            'nohp' => $this->nohp,
            'nowa' => $this->nowa,
            'user' => $this->user->name,
        ];
    }

    public function with($request)
    {
        return ['status' => 'Succes'];
    }
}
