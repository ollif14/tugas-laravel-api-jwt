<?php

namespace App\Models\Perpus;

use App\Models\Perpus\PinjamanBuku;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    protected $fillable = ['nama', 'nim','fakultas','jurusan','nohp','nowa'];

    public function getRouteKeyName(){
        return 'id';
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function pinjamanBuku(){
        return $this->belongsTo(PinjamanBuku::class);
    }
}
