<?php

namespace App\Models\Perpus;

use App\Models\Perpus\Buku;
use App\Models\Perpus\Mahasiswa;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class PinjamanBuku extends Model
{
    protected $fillable = ['buku_id', 'mahasiswa_id','tanggal_pinjaman','tanggal_akhir_pinjaman','tanggal_pengembalian','status_ontime'];

    public function getRouteKeyName(){
        return 'id';
    }

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function buku(){
        return $this->belongsTo(Buku::class);
    }

    public function mahasiswa(){
        return $this->belongsTo(Mahasiswa::class);
    }
}
